import numpy as np
import csv

def get_data():
    with open('consommation-annuelle-residentielle-par-adresse.csv', 'r') as f:
        reader = csv.reader(f,delimiter=";")
        data = list(reader)
    data = np.array(data)
    data = data[1:, :]
    # 7 = code_commune, 12 = consommation_annuelle_moyenne_par_site_de_l_adresse_mwh, 0 = année
    tab = np.array([[int(int(line[7])/1000), float(line[12])] for line in data])
    return tab